
# LiteAUR

Just another AUR helper


### FEATURES:
*   Lightweight
*   Update system
*   Fast
*   Search command
*   Configuration
## Images:
| | |
| --- | --- |
| ![Big Text](https://cdn.discordapp.com/attachments/492780868973101085/890641081497169920/unknown.png) | ![Search Function](https://cdn.discordapp.com/attachments/492780868973101085/890553511429943357/unknown.png) |
| ![Configuration file](https://cdn.discordapp.com/attachments/492780868973101085/890555401576276018/unknown.png) | ![Help command](https://cdn.discordapp.com/attachments/492780868973101085/890555104917356544/unknown.png) | |
---
### FAQ:<br />
1. **Q:** Why can't my package install because of "ERROR: Could not resolve all dependencies" or "ERROR: 'pacman' failed to install missing dependencies"?<br />**A:** Some of that package's dependencies are most-likely AUR packages, not pacman packages. Simply run `liteaur -i foo foobar` (replace foo and foobar with the dependencies that failed to install)

2. **Q:** How to contribute to LiteAUR or suggest a feature?<br />**A:** Simply fork this project, make your changes, and open a pull/merge request OR make an issue with the feature(s) you want added to LiteAUR! All suggestions and contributions are welcome!

3. **Q:** Why is the search command sometimes not outputting anything?<br />**A:** It either means the search you've put has hit the AURWeb RPC's limit, or that it has no results. 

4. **Q:** Help! I found a bug!<br />**A:** Open an issue describing the bug in detail (if possible). 


